function f=gau(t)
global 
a=normrnd(0.1,1,10000,1);
w=t(:,1);
x=t(:,2);
n=2*length(w);
for i=1:n
f(i)=w'*(x.^i)-mean(a.^i);
end

end