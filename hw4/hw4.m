%% Homework 4
% Rui Wang
 
S = load('hw4data.mat');
global N T X Y Z
X=S.data.X';
Y=S.data.Y';
Z=S.data.Z';
N=S.data.N;
T=S.data.T;

%% q1
% question 1 was to calculate likelihood.
beta0=0.1;
sigma0=1;
gamma0=0;
w0=zeros(20,1)
w0(1)=1;
x0=ones(20,1);
t0=[w0,x0];
optset('newton','maxit',20);optset('newton','showiters',1);
% t=broyden('gau',t0)
% There is some error here and I can not find the problem
% I can not solve the grids and weights for the Gaussian Quadrature
% I only use Monte Carlo Methods to do the following question.

% I dont see you trying to find nodes and weights


%% q2
% I multiple 10^6 and take log of the likelihood function otherwise it is too small
n=100;
sdbeta=normrnd(0,1,100,1);
l2=mont(beta0,sigma0,gamma0,sdbeta)
%      this is not the right likelihood.
%% q3
f1=@(x)mont(x(1),x(2),x(3),sdbeta)
xinitial1=[beta0,sigmab,gamma0];
%  sigmab is not existent here, it returns error. 
options = optimset('Display','Iter','MaxIter',5000,'MaxFunEvals',5000,'TolX',0.000001,'TolFun',0.000001);
[x1,Maxvalue1,exitflag1,output1]= fminsearch(f1,xinitial1,options)
% you were asked to use newton's method without derivative, not simplex
% method. the answer is incorrect.
output1.iterations;
output1.funcCount;
x1
Maxvalue1


%% q4
sdbeta1=mvnrnd([0;0],[1,0;0,1],100);
f2=@(x)mont1(x(1),x(2),x(3),x(4),x(5),x(6),sdbeta1)
% there is no such fiunction mont1. gives me an error.
xinitial2=[0.1,0,1,1,1,0];
loptions = optimset('Display','Iter','MaxIter',5000,'MaxFunEvals',5000,'TolX',0.000001,'TolFun',0.000001);
[x2,Maxvalue2,exitflag2,output2]= fminsearch(f2,xinitial2,options)
output2.iterations;
output2.funcCount;
x2
Maxvalue2
