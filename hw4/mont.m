function l=mont(beta0,sigma,gamma,sdbeta)
n=100;
beta=beta0+sdbeta*sigma;
for i=1:n
M(:,i)=bana(beta(i),gamma,0); 
end
l1=(10^4)*sum(M)/n;
l=-prod(l1);
end