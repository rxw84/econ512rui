function L=bana(beta,gamma,u)
global N T X Y Z
L=ones(N,1);
M1=ones(N,T);
M2=ones(N,T);
M=ones(N,T);

for i=1:N  
   for t=1:T
M1(i,t)=(1+exp(-beta*X(i,t)-gamma*Z(i,t)-u))^(-Y(i,t));  
M2(i,t)=(1-(1+exp(-beta*X(i,t)-gamma*Z(i,t)-u))^(-1))^(1-Y(i,t)); 
M(i,t)=M1(i,t)*M2(i,t); 
    end
 L(i)=prod(M(i,:));
end

end