function [g,dg]=bana(beta)
load('hw3.mat')
g=-sum(-exp(X*beta)+y.*(X*beta));
dgg=sum(repmat(exp(X*beta),1,6).*X-repmat(y,1,6).*X);
dg=dgg';
end