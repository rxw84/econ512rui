%% HOMEWORK 3 
% Rui Wang

%% PART A
Y2=1;
Y1=1.1:0.05:3;
n=length(Y1);
theta=ones(2,n);
for i=1:n
theta(:,i)=MLE(Y1 (i),Y2);
end

plot(Y1,theta(1,:))

%% PART B
%% 
% question 1
load('hw3.mat')
beta0=[0;0;0;0;0;0];
n=length(y);
%%
% Fminunc without derivative
options = optimoptions('fminunc','GradObj','off','Algorithm','quasi-newton','HessUpdate','bfgs');
L=@(beta) -sum(-exp(X*beta)+y.*(X*beta));
[beta1,MLHV1,exitflag1,output1]=fminunc(L,beta0,options);
beta1
output1.iterations
output1.funcCount

%%
% Fminunc with derivative
options = optimoptions('fminunc','GradObj','on','Algorithm','quasi-newton','HessUpdate','bfgs');
[beta2,MLHV2,exitflag2,output2]=fminunc('bana',beta0,options);
output2.iterations
output2.funcCount
%%
% Nelder Mead
options = optimset('MaxIter',2000,'MaxFunEvals',2000,'TolX',eps,'TolFun',eps);
[beta3,MLHV3,exitflag3,output3]=fminsearch(L,beta0,options);
beta3
output3.iterations
output3.funcCount

%%
% BHHH MLE
betaold=beta0;
for i=1:2000
betamle=betaold+(bhhh(betaold)'*bhhh(betaold)/n)^(-1)*sum(bhhh(betaold))'/n;
    if norm(betaold-betamle)<eps;
        break
    else 
        betaold=betamle;
    end 
end
betamle
i
%%
% question2 eigenvalue
e0=eig((bhhh(beta0)'*bhhh(beta0)/n)^(-1))
e1=eig((bhhh(betamle)'*bhhh(betamle)/n)^(-1))
%%
% question 3 NLLS
betaini=beta0;
for j=1:2000
    betanls=betaini-(nls(betaini)'*nls(betaini))^(-1)*nls(betaini)'*(y-exp(X*betaini));
     if norm(betanls-betaini)<eps;
        break
    else 
        betaini=betanls;
    end
end
betanls
j
%%
% question 4 Standard error (Asymptotic variance)
sdmls=(bhhh(betamle)'*bhhh(betamle)/n)^(-1)
sdnls=(nls(betanls)'*nls(betanls)/n)^(-1)
