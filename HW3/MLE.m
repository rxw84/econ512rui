function theta=MLE(Y1,Y2)
theta0=1;

for i=1:1000
   f=log(theta0)-log(Y1)+log(Y2)-psi(theta0);
   Df=1/theta0-trigamma(theta0);
   theta1=theta0-f/Df;
if norm(theta1-theta0)<eps
    break
else 
    theta0=theta1;
end
end
theta=[theta1;theta1/Y1];
end