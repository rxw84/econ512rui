

%QUESTION1
X=[1,1.5,3,4,5,7,9,10];
Y1=-2+0.5.*X
Y2=-2+0.5*X.^2

plot(X,Y1,X,Y2);
xlabel('X');
ylabel('Y');
legend('Y1','Y2');

%QUESTION2
X2=linspace(-10,20,200)'
s=sum(X2)


%QUESTION3
A=[2,4,6;1,7,5;3,12,4];
B=[-2;3;10];
C=A'*B

% ^(-1) is slow, use \ instead, check the answer key for that
D=(A'*A)^(-1)*B

% E is a scalar and should be calculated differently
E=A'*B
F=A;
F(2,:)=[];
F(:,3)=[];
F
x=A^(-1)*B


%QUESTION4


% check answer key for use of kron()
B=blkdiag(A,A,A,A,A)
%QUESTION5
A5=10+5*randn(5,3)

% it is easier and faster to use A = A<10
for i=1:5
   for  j=1:3;
    if A5(i,j)<10
        a5(i,j)=0;
    else 
        a5(i,j)=1;
        end
    end
end
a5=a5
%Question6
Y=csvread('datahw1.csv',0,2);
%[exp,RD,pro,cap]
exp=Y(:,1);
RD=Y(:,2);
pro=Y(:,3);
cap=Y(:,4);
X=[exp,RD,cap];
B=regstats(pro,X,'linear');
coe=B.beta
se=B.tstat.se
t=B.tstat.t
p=B.tstat.pval




