%% Homework2
% Rui Wang
%% question1
% 
demand=@(p) [exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
    exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)))];
p1=[1,1,1]';
q=demand(p1)
q0=1-sum(q) %demand for outside good
%% question2
% The starting value
pa=[1,1,1]';
pb=[0,0,0]';
pc=[0,1,2]';
pd=[3,2,1]';
pini=[pa,pb,pc,pd];

eqm=@(p) [1-p(1)+p(1)*exp(-1-p(1))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
    1-p(2)+p(2)*exp(-1-p(2))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)));
    1-p(3)+p(3)*exp(-1-p(3))/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-p(3)))];
optset('broyden','initi',0);
tic
for k=1:4
[x,fval,flag,it]=broyden(eqm,pini(:,k));
end
x
toc

%%
% It converges and the convergence criteria is feval(eqm,pini)<eps.
%% question3 
d1=1;d2=1; d3=1; %initial slope for secant method

for k=1:4
 p=pini(:,k); %initial value
 t=1; 
 tic
while norm(eqm(p))>10^(-10) %try to use the same tolerance as in broyden, there is is default at 1e-8

foc1=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-x)+exp(-1-p(2))+exp(-1-p(3)));
foc2=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-x)+exp(-1-p(3)));
foc3=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-x));
    
x0=p;
%%
% We use secant method to get the root of foc1, foc2 and foc3

% root for foc1
while abs(foc1(x0(1)))>eps  
    x1(1)=x0(1)-foc1(x0(1))/d1;
    d1=(foc1(x1(1))-foc1(x0(1)))/(x1(1)-x0(1));
    x0(1)=x1(1);
end
% root for foc2
while abs(foc2(x0(2)))>eps
    x1(2)=x0(2)-foc2(x0(2))/d2;
    d2=(foc2(x1(2))-foc2(x0(2)))/(x1(2)-x0(2));
    x0(2)=x1(2);
end
% root for foc3
while abs(foc3(x0(3)))>eps
    x1(3)=x0(3)-foc3(x0(3))/d3;
    d3=(foc3(x1(3))-foc3(x0(3)))/(x1(3)-x0(3));
    x0(3)=x1(3);
end
 p=x0; 
 t=t+1;  %counting the iteration times
end
toc
T(k)=t;
X(:,k)=p;
end
T
X
%%
% We can see that Gauss-Jacobi method is slower than broyden since
% Gauss-Jacobi has two loops. It has to solve the root for three functions
% in inner loop and then subsititute the initial value in the outer loop until it converges.
%% question4
% 
for k=1:4
t=1;
pa0=pini(:,k);
pa1=1./(1.-demand(pa0));
tic
while norm(pa1-pa0)>eps
   pa0=pa1;
   pa1=1./(1.-demand(pa0));
   t=t+1;
end
toc
X(:,k)=pa1;
T(k)=t;
end
T
X
%%
% It converges and it is much faster than the two other methods since the iteration is very simple. 

%% question5
d1=1;d2=1; d3=1; %the slope is one for the initial point

for k=1:4
 p=pini(:,k);
 t=1;
tic
while norm(eqm(p))>10^(-10)
foc1=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-x)+exp(-1-p(2))+exp(-1-p(3)));
foc2=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-x)+exp(-1-p(3)));
foc3=@(x) 1-x+x*exp(-1-x)/(1+exp(-1-p(1))+exp(-1-p(2))+exp(-1-x));
    
x0=p;
%x1=[x0(1)-foc1(x0(1));x0(2)-foc2(x0(2));x0(3)-foc3(x0(3))];
%%
% we still use the secant method but only take a single secant step. We can
% see that there is no loop here.

%  root for foc1
   x1(1)=x0(1)-foc1(x0(1))/d1;
   d1=(foc1(x1(1))-foc1(x0(1)))/(x1(1)-x0(1));
   x0(1)=x1(1); 
% root for foc2
    x1(2)=x0(2)-foc2(x0(2))/d2;
    d2=(foc2(x1(2))-foc2(x0(2)))/(x1(2)-x0(2));
    x0(2)=x1(2); 
% root for foc3
    x1(3)=x0(3)-foc3(x0(3))/d3;
    d3=(foc3(x1(3))-foc3(x0(3)))/(x1(3)-x0(3));
    x0(3)=x1(3);
    
 p=x0; 
 t=t+1;
end
toc
T(k)=t;
X(:,k)=p;
end
T
X